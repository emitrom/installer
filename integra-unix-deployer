#!/usr/bin/perl

use strict;
use warnings;

use File::Copy;
use Term::ReadKey;
use FindBin qw($Bin);

my $INSTALLER_DIR         = '/tmp/installer';
my $LICENSE               = "$INSTALLER_DIR/conf/LICENSE";
my $TMP_INTEGRA_BUILD_DIR = '/tmp/integra_builds';
my $INSTALL_DEFAULT_DIR   = '/usr/sbin';
my $SERVICE_DIR           = '/etc/init.d';
my $BUILD_URL             = 'https://go.emitrom.com';
my $BUILD_NAME            = 'integra-reactor.zip';
my $PROVIDER_CFG_DIR      = '/etc/integra';

my $build;
my $vCenterUrl;
my $vCenterUsername;
my $vCenterPassword;
my $vCenterVersions = "'5.1.0' '5.5.0', '6.0.0'";
my $vCenterVersion;
my $vCenterInstall = 0;
my $coreInstall = 0;

sub main {
   clean();
   printWelcome();
}

main();

sub clean {

   if (-d $TMP_INTEGRA_BUILD_DIR) {
      `rm -rf $TMP_INTEGRA_BUILD_DIR`;
   }

   `mkdir $TMP_INTEGRA_BUILD_DIR`;

}

sub downloadBuild {

   print "Starting to download $BUILD_URL/$build/$BUILD_NAME to $TMP_INTEGRA_BUILD_DIR/$BUILD_NAME \n";

   `wget $BUILD_URL/$build/build/$BUILD_NAME -O $TMP_INTEGRA_BUILD_DIR/$BUILD_NAME --no-check-certificate`;

   print "Finished downloading $BUILD_URL/$build/$BUILD_NAME to $TMP_INTEGRA_BUILD_DIR/$BUILD_NAME\n\n";

}

sub showVersions {

   print "--------- \n";
   print " Version \n";
   print "--------- \n";

   my @buildVersions = ();
   open(BUILD_H,"curl -s $BUILD_URL --insecure |" ) || die "Failed: $!\n";
   while ( <BUILD_H> ) {
      my $builds = $_;      
      $builds =~ s/\[//;
      $builds =~ s/\]//;
      $builds =~ s/"//g;
      chomp($builds);

      @buildVersions = split(/,/, $builds);
      @buildVersions = sort @buildVersions;
      foreach (@buildVersions) {
         print "$_ \n";
      }

   }

   print "\nSelect version to install: ";
   $build = <STDIN>;
   chomp($build);

   my $valid = grep { $_ eq $build } @buildVersions;

   print "\nExiting ... Please provide a valid version number\n\n" and exit if !$valid;

   system('more', $LICENSE);
   print "\n Do you accept the license agreement? [y/n]\n";
   print "> ";
   my $acceptLicense = <STDIN>;
   chomp($acceptLicense);

   if ($acceptLicense eq 'y' || $acceptLicense eq 'Y') {
      showMenu();
   } else {
      print "You must accept the license in order to proceed with the installation \n";
      exit;
   }      

}

sub getVcenterOptions {

   print "Enter vCenter Version $vCenterVersions: ";
   $vCenterVersion = <STDIN>;
   chomp($vCenterVersion);

   print "Enter vCenter URL. Example: [https://vcenter.emitrom.com]: ";
   $vCenterUrl = <STDIN>;
   chomp($vCenterUrl);

   print "Enter vCenter username: ";
   $vCenterUsername = <STDIN>;
   chomp($vCenterUsername);

   ReadMode('noecho');
   print "Enter vCenter password: ";
   $vCenterPassword = <STDIN>;
   chomp($vCenterPassword);
   ReadMode('normal');

   $vCenterInstall = 1;
}

sub installReactorAndProviders {

   downloadBuild(); 

   if (!-d $PROVIDER_CFG_DIR) {
      `mkdir $PROVIDER_CFG_DIR`;
   }

   if (!-d $TMP_INTEGRA_BUILD_DIR) {
      print "Unable to find integra repository under $TMP_INTEGRA_BUILD_DIR\n\n";
      exit;
   }

   #
   # Unzip initial zip
   #
   print "Start unzip $TMP_INTEGRA_BUILD_DIR/$BUILD_NAME to $TMP_INTEGRA_BUILD_DIR \n";
   `unzip -o $TMP_INTEGRA_BUILD_DIR/$BUILD_NAME -d $TMP_INTEGRA_BUILD_DIR`;
   `unzip -o $TMP_INTEGRA_BUILD_DIR/$BUILD_NAME -d $TMP_INTEGRA_BUILD_DIR`;
   print "Finished unzip $TMP_INTEGRA_BUILD_DIR/$BUILD_NAME to $TMP_INTEGRA_BUILD_DIR \n\n";

   #
   # Get array individual executables
   #
   my $TMP_H;
   opendir($TMP_H, $TMP_INTEGRA_BUILD_DIR) || die "can't opendir $TMP_INTEGRA_BUILD_DIR $!";
   my @execs = grep { /.*-bin.zip$/ } readdir($TMP_H);
   closedir $TMP_H; 

   #
   # unzip all executables
   #
   foreach (@execs) {
      my $execFile = $TMP_INTEGRA_BUILD_DIR . '/' . $_;
      print "Start unzip of $execFile \n";
      `unzip -o $execFile -d $TMP_INTEGRA_BUILD_DIR`;
      print "Finished unzip of $execFile\n";
   }
   print "\n";

   #
   # copy cli
   #
   my $cliPath = `ls $TMP_INTEGRA_BUILD_DIR/cli-*.jar`;
   chomp($cliPath);

   print "Start copy of CLI from $cliPath to $INSTALL_DEFAULT_DIR\n";   

   # bash cli wrapper
   copy("$INSTALLER_DIR/integra-cli", $INSTALL_DEFAULT_DIR) or die "Unable to copy CLI bash file: $!";
   chmod 0755, "$INSTALL_DEFAULT_DIR/integra-cli" or die $!;

   copy($cliPath, $INSTALL_DEFAULT_DIR) or die "Unable to copy CLI executable (jar): $!";
   chmod 0755, $cliPath or die $!;

   print "Finished copy of CLI from $TMP_INTEGRA_BUILD_DIR/cli-*-uber.jar to $INSTALL_DEFAULT_DIR\n";   

   #
   # copy execs to /usr/sbin
   #
   opendir($TMP_H, $TMP_INTEGRA_BUILD_DIR) || die "can't opendir $TMP_INTEGRA_BUILD_DIR $!";
   my @execDirs = grep ! /^\./, readdir($TMP_H);
   closedir $TMP_H; 

   #
   # iterate over unzipped dirs
   #
   foreach my $dir (@execDirs) {

      my $execDir = $TMP_INTEGRA_BUILD_DIR . '/' . $dir;

      if (-d $execDir) {

         print "processing provider directory: $execDir\n";
     
         my $providerConf = $execDir . '/' . 'provider.conf';
    
         opendir(my $UBER_H, $execDir) || die "can't opendir $execDir $!";
         my @uberJars = grep { /(.*-uber.jar)$/ } readdir($UBER_H);
         closedir $UBER_H; 

         #
         # should only have one jar but if we ever add more than one
         # uber jar, this should handle it transparently.
         #
         foreach (@uberJars) {

            my $uberJar = $execDir . '/' . $_;

            eval {

               print "copy $uberJar to $INSTALL_DEFAULT_DIR \n";
               copy($uberJar, $INSTALL_DEFAULT_DIR) or die "Copy failed: $!";

               if ($uberJar =~ /.*\/(.*.jar$)/) {
                  print "chmod 755 $INSTALL_DEFAULT_DIR/$1 \n";
                  chmod 0755, "$INSTALL_DEFAULT_DIR/$1" or die $!;
               }

            };
            if ($@) {
               print $@;
            }

         }

         #
         # if the provider configuration file doesn't exist under /etc copy it there.
         #
         my $confFile = "$PROVIDER_CFG_DIR/$dir.conf";
         if (-e $confFile) {
            print "found and skipping configuration file $confFile\n";
         } else {
            #
            # REST has a different config file, rest.conf in this case.
            #
            if ($confFile =~ /rest/i) {
               my $restConf = $execDir . '/' . 'rest.conf';
               print "copy $restConf to $confFile\n";
               copy($restConf, $confFile) || die $!; 	
            } else {
               #
               # Copy providers configuration files
               #
               print "copy $providerConf to $confFile\n";
               copy($providerConf, $confFile) || die $!; 	
            }
         }
         print "\n";

      }
   }

   installAndConfigureServices();

   $coreInstall = 1;

}

sub start {
   system('service', 'integra-reactor', 'start');
   system('service', 'integra-providers', 'start');
}

sub stop {
   system('service', 'integra-reactor', 'stop');
   system('service', 'integra-providers', 'stop');
}

sub deployVcenter {

   # integra-vcenter-deploy.jar options
   # 
   # -k,--pkg <pluginPackage>       Path of the plugin-package.xml file
   # -p,--passwd <password>         vCenter password
   # -s,--server <vCenter Server>   vCenter Server IP or hostname
   # -t,--tag <tag>                 Git tag
   # -u,--user <username>           vCenter username

   print "Getting plugin-package.xml ready for vCenter\n";   
   `sed 's/id="com.vmware.vsphere.client" version=""/id="com.vmware.vsphere.client" version="$vCenterVersion"/g' $Bin/conf/plugin-package-template.xml > $Bin/conf/plugin-package.xml`;
   print "Finished preparing plugin-package.xml ready for vCenter\n";   

   print "Wait while we deploy vCenter\n";
   eval {
      print "Executing: java -jar $Bin/integra-vcenter-deploy.jar --pkg $Bin/conf/plugin-package.xml --passwd ******** --server $vCenterUrl/sdk --build $build --user $vCenterUsername \n";
      `java -jar $Bin/integra-vcenter-deploy.jar --pkg $Bin/conf/plugin-package.xml --passwd $vCenterPassword --server $vCenterUrl/sdk --build $build --user $vCenterUsername`;
   };

}

sub installAndConfigureServices {

   print "Start copying conf/integra-providers service to $SERVICE_DIR\n";
   copy('conf/integra-providers', $SERVICE_DIR) || die $!;
   print "Finished copying conf/integra-providers service to $SERVICE_DIR\n\n";

   print "Start copying conf/integra-reactor service to $SERVICE_DIR\n";
   copy('conf/integra-reactor', $SERVICE_DIR) || die $!;
   print "Finished copying conf/integra-reactor service to $SERVICE_DIR\n\n";

   print "Start adding integra-providers service\n";
   `chkconfig --add integra-providers`;
   chmod 0755, "$SERVICE_DIR/integra-providers" or die $!;
   print "Finished adding integra-providers service\n\n";

   print "Start adding integra-reactor service\n";
   `chkconfig --add integra-reactor`;
   chmod 0755, "$SERVICE_DIR/integra-reactor" or die $!;
   print "Finished adding integra-reactor service\n\n";

   stop();

   sleep 10;

   start();

   print "The integra services are starting, please wait ...\n";
   sleep 15;

   #addProviders();

}

sub addProviders {

   print "Add all providers to the reactor [y/n] ";
   my $isAdd = <STDIN>;
   chomp($isAdd); 

   if ($isAdd eq 'y') {

      print "Starting to add providers ...\n\n";

      my $TMP_H;
      opendir($TMP_H, "$PROVIDER_CFG_DIR") || die "can't opendir $PROVIDER_CFG_DIR $!";
      my @providerCfgs = grep { /.*-provider.conf$/ } readdir($TMP_H);
      closedir $TMP_H;

      foreach my $providerCfg (@providerCfgs) {
         my $providerName = $providerCfg;
         $providerName =~ s/\.conf//g;
         eval {
            my $port;
            open ('CFG_FILE', "$PROVIDER_CFG_DIR/$providerCfg") || die $!;
            while (<CFG_FILE>) {
               if ($_ =~ /^#/) {
                  next;
               }
               if ($_ =~ /.*=.*/) {
                  my ($key, $value) = split('=', $_);
                  chomp($key);
                  chomp($value);
                  if ($key eq 'port') {
                     $port = $value;
                     print "Adding provider with hostname: localhost, port: $port and name: $providerName \n";
                     `$INSTALL_DEFAULT_DIR/integra-cli -hostname localhost -protocol https -username admin -password integra -addProvider -name $providerName -providerHostname localhost -providerPort $port -timeout 0`;
                  } 
               }
            }
         };
         if ($@) {
            print $@, "\n";
         }
      }
      print "\n";

      print "Finished adding providers ...\n";

   }

}

sub printWelcome {

print <<EOF;

   Welcome to the Integra Installer!

EOF

   system("cat", "conf/integra.logo");

   showVersions();

}

sub showMenu {

print <<EOF;

Enter option: 

   1. Install vCenter Plugin
   2. Install providers and reactor
   3. Exit

EOF

   my $option = <STDIN>;
   chomp($option);

   if ($option == 1) {
      getVcenterOptions();
      deployVcenter();
      showMenu();
   } elsif ($option == 2) {
      installReactorAndProviders();
      showMenu();
   } elsif ($option == 3) {
     exitInstaller(); 
   } else {
      showMenu();
   }

}

sub exitInstaller {

   if ($vCenterInstall || $coreInstall) {

print <<EOF;

   Thank you for installing Integra!

   ========================
   Post Installation Steps:
   ========================
EOF

   }

   if ($coreInstall) {
print <<EOF;

   ========================
   Reactor Steps:        
   ========================

   - To verify all providers and reactor are running use:

      service integra-providers status
      service integra-reactor status
EOF
   }

   if ($vCenterInstall) {

print <<EOF;

   ========================
   vCenter Steps:
   ========================

   - Login to vCenter and click on the Integra icon.

   - Enter for the vCenter REST Server the IP:PORT of the reactor using https.

      Example: https://localhost:8443
      username: admin
      password: integra
EOF
   }

print <<EOF;

   - For any issues or questions, please contact us at support\@emitrom.com

EOF

   exit(0);  

}
